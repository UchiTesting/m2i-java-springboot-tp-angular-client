import { ICompte } from "../interfaces/ICompte"

export class Compte {
    numero;
    label;
    solde;

    constructor(label: string, solde: number = 10, numero: number = 0) {
        this.label = label;
        this.solde = solde;
        this.numero = numero;
    }

    toObject(): ICompte {
        return {
            numero: this.numero,
            label: this.label,
            solde: this.solde,
        }
    }
}
