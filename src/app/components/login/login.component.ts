import { Component, OnInit } from '@angular/core';
import { LoginRequest } from 'src/app/common/data/loginRequest';
import { LoginResponse } from 'src/app/common/data/loginResponse';
import { LoginService } from 'src/app/common/service/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginRequest = new LoginRequest();
  message: string = "";


  constructor(private loginService: LoginService) { }

  ngOnInit(): void {
  }

  doLogin() {
    this.loginService.postLogin$(this.loginRequest).subscribe(
      {
        next: (res: LoginResponse) => this.processPostLogin(res),
        error: err => {
          sessionStorage.setItem("token", "null");
          console.log(err);
          this.message = "Echec Login";
        }
      });
  }

  processPostLogin(res: LoginResponse) {
    sessionStorage.setItem("token", res.token);
    this.message = res.message;
    console.log(JSON.stringify(res));

  }
}
