import { Component, OnInit } from '@angular/core';
import { Compte } from 'src/app/common/data/compte';
import { Virement } from 'src/app/common/data/virement';
import { CompteService } from 'src/app/common/service/compte.service';


@Component({
  selector: 'app-comptes',
  templateUrl: './comptes.component.html',
  styleUrls: ['./comptes.component.css']
})

export class ComptesComponent implements OnInit {

  numClient = 0;
  listeComptes: Compte[] = [];
  virement: Virement = new Virement();

  constructor(private compteService: CompteService) {
  }

  ngOnInit(): void {
  }

  declencherVirement() {
    this.compteService.postVirement$(this.virement).subscribe(
      {
        next: (virementEffectue: Virement) => this.processPostVirement(virementEffectue),
        error: err => console.log(err)
      });
  }

  processPostVirement(virementEffectue: Virement) {
    this.virement = virementEffectue;

    if (virementEffectue.status) {
      this.searchComptes();
    }
  }

  searchComptes() {
    this.compteService.rechercherComptesDuClient$(this.numClient).subscribe(
      {
        next: (comptes: Compte[]) => this.listeComptes = comptes,
        error: err => console.log(err)

      });
  }

}
