import { Component, OnInit } from '@angular/core';
import { Client } from 'src/app/common/data/client';
import { ClientService } from 'src/app/common/service/client.service';

@Component({
  selector: 'app-clients',
  templateUrl: './clients.component.html',
  styleUrls: ['./clients.component.css']
})
export class ClientsComponent implements OnInit {
  client: Client = new Client();
  message: string = "";

  constructor(private clientService: ClientService) { }

  ngOnInit(): void {
  }

  ajoutClient() {
    this.clientService.postClient$(this.client).subscribe(
      {
        next: (client: Client) => { this.processPostClient(client) },
        error: (err) => {
          this.message = "Echec récupération du client";
          console.log(err);
        }
      }
    );
  }

  processPostClient(client: Client) {
    this.client = client;
    this.message = "Client ajouté côté serveur";
  }
}
