export interface ICompte {
    numero: number;
    label: string;
    solde: number;
}
