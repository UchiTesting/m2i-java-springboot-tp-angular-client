import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { LoginRequest } from '../data/loginRequest';
import { LoginResponse } from '../data/loginResponse';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  baseUrl = "http://localhost:8080/appliSpringBoot/api-login/public/login";

  constructor(private http: HttpClient) { }

  // Le symbole '$' final est une convention pour indiquer un Observable
  // public par défaut
  postLogin$(req: LoginRequest): Observable<LoginResponse> {
    let url = this.baseUrl;
    return this.http.post<LoginResponse>(url, req);
  }
}
