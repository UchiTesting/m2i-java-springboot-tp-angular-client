import { TestBed } from '@angular/core/testing';

import { LoginService } from "./login.service"

describe('CompteService', () => {
  let login: LoginService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    login = TestBed.inject(LoginService);
  });

  it('should be created', () => {
    expect(login).toBeTruthy();
  });
});
