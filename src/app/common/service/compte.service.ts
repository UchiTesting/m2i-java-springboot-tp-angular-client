import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Compte } from '../data/compte';
import { Virement } from '../data/virement';

@Injectable({
  providedIn: 'root'
})
export class CompteService {

  baseUrl = "http://localhost:8080/appliSpringBoot/bank-api/compte";

  constructor(private http: HttpClient) { }

  // Le symbole '$' final est une convention pour indiquer un Observable
  // public par défaut
  rechercherComptesDuClient$(numClient: number): Observable<Compte[]> {
    let url = this.baseUrl + "?numClient=" + numClient;
    return this.http.get<Compte[]>(url);
  }

  postVirement$(virement: Virement): Observable<Virement> {
    let url = this.baseUrl + "/virement";
    return this.http.post<Virement>(url, virement);
  }
}
