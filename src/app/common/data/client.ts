export class Client {

    // public permet d'automatiser la création des attributs du même nom.
    constructor(
        public numero: number = 0,
        public prenom: string = "",
        public nom: string = "",
        public adresse: string = "",
        public email: string = "") {
    }
}