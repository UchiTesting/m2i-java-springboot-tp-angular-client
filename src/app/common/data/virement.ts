import { ICompte } from "../../interfaces/ICompte"

export class Virement {

    // public permet d'automatiser la création des attributs du même nom.
    constructor(public montant: number = 0, public numCptDebt: string = "", public numCptCred: string = "", public status: boolean | undefined = undefined, public message: string | undefined = undefined, public date: string | undefined = undefined) {
    }
}