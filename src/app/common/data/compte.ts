import { ICompte } from "../../interfaces/ICompte"

export class Compte {

    // public permet d'automatiser la création des attributs du même nom.
    constructor(public numero: number = 0, public label: string = "", public solde: number = 0) {
    }
}