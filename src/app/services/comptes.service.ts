import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { ICompte } from '../interfaces/ICompte';
import { COMPTES } from '../mocks/comptes-mock';

@Injectable({
  providedIn: 'root'
})
export class ComptesService {

  constructor() { }

  listeComptes: Array<ICompte> = COMPTES;

  getComptes(): Observable<ICompte[]> {
    return of(this.listeComptes);
  }
}
