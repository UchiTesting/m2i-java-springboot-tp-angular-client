import { Compte } from "../implementations/compte";
import { ICompte } from "../interfaces/ICompte";

export const COMPTES: ICompte[] = [
    new Compte('Compte 1', 10.0, 1),
    new Compte('Compte 2', 10.0, 2),
    new Compte('Compte 3', 10.0, 3)
]
